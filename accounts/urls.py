from django.urls import path
from accounts.views import LoginView, LogoutView, SignUpView


urlpatterns = [
    path("login/", LoginView, name="login"),
    path("logout/", LogoutView, name="logout"),
    path("signup/", SignUpView, name="signup")
]
