from django.urls import path
from receipts.views import view_receipts, create_receipt_view, view_categories, view_accounts, create_category_view, create_account_view


urlpatterns = [
    path("", view_receipts, name="home"),
    path("create/", create_receipt_view, name="create_receipt"),
    path("categories/", view_categories, name="category_list"),
    path("accounts/", view_accounts, name="account_list"),
    path("categories/create/", create_category_view, name="create_category"),
    path("accounts/create/", create_account_view, name="create_account"),
]
